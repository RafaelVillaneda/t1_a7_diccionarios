'''
Created on 30/09/2020

@author: rafae
'''
import datetime
from test.test_struct import integer_codes
class Alumno:
    def __init__(self,nombre,edad,carrera,fechaInscripcion):
        self.nombre=nombre
        self.edad=edad
        self.carrera=carrera
        self.fechaInscripcion=fechaInscripcion
    def __str__(self):
        return f"nombre: {self.nombre} edad: {self.edad} carrera: {self.carrera} con fecha de inscripcion {self.fechaInscripcion}"

class listaAlumnos:
    def __init__(self):
        self.mapa={}
    def agregarAlumno(self,fol,alum=Alumno):
        self.mapa.setdefault(alum,fol)
    def vaciarlista(self):
        self.mapa.clear()
    def mostrarAlumnosCarrera(self,op):
        for x in self.mapa:
            x=self.mapa.get(x,"Dato no encontrado")
            if(x.carrera=="ISC" and op=="1"):
                print(x.nombre)
            elif(x.carrera=="IIA" and op=="2"):
                print(x.nombre)
            elif(x.carrera=="LM" and op=="3"):
                print(x.nombre)
            elif(x.carrera=="LA" and op=="4"):
                print(x.nombre)
            elif(x.carrera=="CP" and op=="5"):
                print(x.nombre)
            
    def calcularPromedioEdades(self):
        suma=0
        for a in self.mapa:
            poci=self.mapa.get(a,"No encontrado")
            suma=suma+poci.edad
        total=0
        if(len(self.mapa)==0):
            total=1
        else:
            total=len(self.mapa)
        return suma/total
    def mostrarAlumnosInscritosdespuesFecha(self):
        fecha1 = datetime.datetime(2016,8,10)
        for a in self.mapa:
            a=self.mapa.get(a,"Dato no encontrado")
            fechas=[]
            print(a.fechaInscripcion.split("/"))
            fechas=a.fechaInscripcion.split("/")
            fechas2=[]
            i=0
            for i in range(i,len(fechas)):
                fechas2.append(int(fechas[i]))

            dia=fechas2[0]
            mes=fechas2[1]
            año=fechas2[2]
            fecha2=datetime.datetime(año,mes,dia)
            if(fecha1 < fecha2):
                print(a.nombre)
        

op=""
folio=1
dic=listaAlumnos()
while(op!="F"):
    print("Elige la opcion que desees: ")
    print("A) Llenar lista")
    print("B) Vaciar lista")
    print("C) Mostrar por carrera")
    print("D) Calcular promedio edades")
    print("E) Mostrar los alumnos que se inscribieron después de la fecha indicada (10/08/2016)")
    print("F) Salir")
    op=input().upper()
    if(op=="A"):
        bandera=True
        nombre=input("Ingresa el nombre del alumno: ")
        edad=0
        while(bandera==True):
            try:
                edad=int(input("Ingresa la edad del alumno: "))
                bandera=False
            except ValueError:
                bandera=True
                print("Ingresa un numero entero")
        carrera=input("Ingresa la carrera: ").upper()
        fechaInscripcion=input("Ingresa la fecha de inscripcion con formato (dd/mm/aaaa): ")
        alumno=Alumno(nombre,edad,carrera,fechaInscripcion)
        dic.agregarAlumno(alumno,folio)
        folio=folio+1
        print(dic.mapa.keys())
    elif(op=="B"):
        dic.vaciarlista()
        print("Se vacio la lista")
    elif(op=="C"):
        print("1-ISC")
        print("2-IIA")
        print("3-LM")
        print("4-LA")
        print("5-CP")
        op2=input("Que listado de alumnos quieres que mostremos: ")
        dic.mostrarAlumnosCarrera(op2)
    elif(op=="D"):
        print("El promedio de las edades es de: ",dic.calcularPromedioEdades())
    elif(op=="E"):
        print("Los alumnos que se inscribieron despues son: ")
        dic.mostrarAlumnosInscritosdespuesFecha()
    elif(op=="F"):
        print("Saliendo....")
    else:
        print("Opcion ni disponible")
        
        
        
        